#!/bin/bash -xv

# rebuilds node-odm from updated base images

# exit when any command fails
set -e

# get date & time for image tagging
# https://www.cyberciti.biz/faq/unix-linux-appleosx-bsd-shell-appending-date-to-filename/
# https://www.cyberciti.biz/faq/linux-unix-formatting-dates-for-display/
_now=$(date +"%b%d%Y%H%M")
#_file="/nas/backup_$_now.sql"


# from scripts dir
cd ../OpenDroneMap.GEI
git checkout development-amd64
#git pull

# build odm-base image
#echo "Starting build of odm-base image"
#docker build --tag geovisionenvironmental/odm-base-amd64:latest -f Dockerfile.odm.base .
#docker tag geovisionenvironmental/odm-base-amd64:latest geovisionenvironmental/odm-base-amd64:$_now

# update docker source-build image
# docker run --rm --name odm-source-builds-amd64 geovisionenvironmental/odm-source-builds-amd64:latest
# update scripts in container
# docker exec
# run update source builds script
# docker exec 
#docker tag geovisionenvironmental/odm-source-builds-amd64:latest geovisionenvironmental/odm-source-builds-amd64:$_now

# build OpenDroneMap using multi-stage
echo "Starting build of odm multi-stage image"
docker build --no-cache --tag geovisionenvironmental/odm-amd64-dev-multi:latest -f Dockerfile.odm.multi-stage .
docker tag geovisionenvironmental/odm-amd64-dev-multi:latest geovisionenvironmental/odm-amd64-dev-multi:$_now

# build node-odm
echo "Starting build of node-odm image"
cd ../node-OpenDroneMap.GEI
docker build --no-cache --tag geovisionenvironmental/opendronemap-private:nodeodm-amd64-dev-multi-latest -f Dockerfile.slim .
docker tag geovisionenvironmental/opendronemap-private:nodeodm-amd64-dev-multi-latest geovisionenvironmental/opendronemap-private:nodeodm-amd64-dev-multi-$_now

echo "Finished up with fresh node-OpenDroneMap Docker app build"




