#!/bin/bash -xv

# builds fresh node-odm from new base images

# exit when any command fails
set -e

# get date & time for image tagging
# https://www.cyberciti.biz/faq/unix-linux-appleosx-bsd-shell-appending-date-to-filename/
# https://www.cyberciti.biz/faq/linux-unix-formatting-dates-for-display/
_now=$(date +"%b%d%Y%H%M")
#_file="/nas/backup_$_now.sql"


# from scripts dir
cd ../opendronemap
# build odm-base image
echo "Starting build of odm-base image"
docker build --tag geovisionenvironmental/odm-base-amd64:latest -f Dockerfile.odm.base .
docker tag geovisionenvironmental/odm-base-amd64:latest geovisionenvironmental/odm-base-amd64:$_now
# build docker source-build container
docker build --no-cache --tag geovisionenvironmental/odm-source-builds-amd64:latest -f Dockerfile.odm.source-builds .
docker tag geovisionenvironmental/odm-source-builds-amd64:latest geovisionenvironmental/odm-source-builds-amd64:$_now
# build OpenDroneMap using multi-stage
docker build --no-cache --tag geovisionenvironmental/odm-amd64-dev-multi:latest -f Dockerfile.odm.multi-stage .
docker tag geovisionenvironmental/odm-amd64-dev-multi:latest geovisionenvironmental/odm-amd64-dev-multi:$_now

# build node-odm
echo "Starting build of node-odm image"
cd ../node-opendronemap
docker build --no-cache --tag geovisionenvironmental/opendronemap-private:nodeodm-amd64-dev-multi-latest -f Dockerfile.slim.fitlet2 .
docker tag geovisionenvironmental/opendronemap-private:nodeodm-amd64-dev-multi-latest geovisionenvironmental/opendronemap-private:nodeodm-amd64-dev-multi-$_now

echo "Finished up with fresh node-OpenDroneMap Docker app build"




