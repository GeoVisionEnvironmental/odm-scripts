#!/bin/bash -vx

# script is CP to image when built, located /usr/local/src/scripts/odm_source-builds_update.sh


# update VTK
cd /usr/local/src/VTK/VTK
git checkout master && git pull && git checkout v8.1.1
cd ../VTK-build
cmake ../VTK \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr/local \
    -DVTK_SMP_IMPLEMENTATION_TYPE=TBB \
    -DCMAKE_BUILD_TYPE=Release \
    -DVTK_Group_Rendering=OFF \
    -DBUILD_TESTING=OFF && \
    make -j$(nproc) && make install

# update CMVS-PMVS
cd /usr/local/src/CMVS-PMVS
git checkout master && git pull
cd ./CMVS-PMVS/program/build && cmake .. && make -j$(nproc) && make install

# update PointCloudLibrary
cd /usr/local/src/pcl
# testing w/ pcl master
git checkout master && git pull 
#&& git checkout pcl-1.8.1
cd build
cmake .. \
    -DBUILD_features=ON \
    -DBUILD_filters=ON \
    -DBUILD_geometry=ON \
    -DBUILD_keypoints=ON \
    -DBUILD_outofcore=OFF \
    -DBUILD_people=OFF \
    -DBUILD_recognition=OFF \
    -DBUILD_registration=ON \
    -DBUILD_sample_consensus=ON \
    -DBUILD_segmentation=ON \
    -DBUILD_surface_on_nurbs=OFF \
    -DBUILD_tools=OFF \
    -DBUILD_tracking=ON \
    -DBUILD_visualization=OFF \
    -DWITH_QT=OFF \
    -DBUILD_OPENNI=OFF \
    -DBUILD_OPENNI2=OFF \
    -DWITH_OPENNI=OFF \
    -DWITH_OPENNI2=OFF \
    -DWITH_FZAPI=OFF \
    -DWITH_LIBUSB=OFF \
    -DWITH_PCAP=OFF \
    -DWITH_PXCAPI=OFF \
    -DCMAKE_BUILD_TYPE=Release \
    -DPCL_VERBOSITY_LEVEL=Error && \
    make -j$(nproc) && make install

# update MVS-Texturing
cd /usr/local/src/mvs-texturing
git checkout master && git pull
# default ODM -RESEARCH=OFF flag not in current ccmake, removed, ?causing error
cd build
cmake .. \
    -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_INSTALL_PREFIX=/usr/local \
    && make -j$(nproc) && make install

# update GFlags
cd /usr/local/src/gflags
git checkout master && git pull
cd build
cmake .. \
    -DCMAKE_BUILD_TYPE:STRING=Release \
    && make -j$(nproc) && make install
    
# update Ceres
cd /usr/local/src/ceres-solver
git checkout master && git pull && git checkout 1.14.0
cd build
cmake .. \
    -DCMAKE_C_FLAGS=-fPIC \ 
    -DCMAKE_CXX_FLAGS=-fPIC \
    -DBUILD_EXAMPLES=OFF \
    -DBUILD_TESTING=OFF \
    && make -j$(nproc) && make install
    
# update OpenCV
cd /usr/local/src/opencv
git checkout master && git pull && git checkout 3.4.1
cd build
cmake .. \
    -DCMAKE_BUILD_TYPE:STRING=Release \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr/local \
	-DENABLE_PRECOMPILED_HEADERS=OFF \
    -DCMAKE_BUILD_TYPE=Release \
    -DBUILD_PNG=ON \
    -DBUILD_TIFF=ON \
    -DBUILD_TBB=ON \
    -DBUILD_JPEG=ON \
    -DBUILD_JASPER=OFF \
    -DBUILD_ZLIB=OFF \
    -DBUILD_opencv_core=ON \
    -DBUILD_opencv_imgproc=ON \
    -DBUILD_opencv_highgui=ON \
    -DBUILD_opencv_video=ON \
    -DBUILD_opencv_ml=ON \
    -DBUILD_opencv_features2d=ON \
    -DBUILD_opencv_calib3d=ON \
    -DBUILD_opencv_contrib=ON \
    -DBUILD_opencv_flann=ON \
    -DBUILD_opencv_objdetect=ON \
    -DBUILD_opencv_photo=ON \
    -DBUILD_opencv_legacy=ON \
    -DBUILD_opencv_python=ON \
    -DWITH_VTK=OFF \
    -DWITH_EIGEN=OFF \
    -DWITH_OPENNI=OFF \
    -DBUILD_DOCS=OFF \
    -DBUILD_opencv_apps=OFF \
    -DBUILD_opencv_videostab=OFF \
    -DBUILD_opencv_nonfree=OFF \
    -DBUILD_opencv_stitching=OFF \
    -DBUILD_opencv_world=OFF \
    -DBUILD_opencv_superres=OFF \
    -DBUILD_opencv_java=OFF \
    -DBUILD_opencv_ocl=OFF \
    -DBUILD_opencv_ts=OFF \
    -DWITH_OPENCL=ON \
    -DWITH_CUDA=OFF \
    -DBUILD_opencv_gpu=ON \
    -DBUILD_EXAMPLES=OFF \
    && make -j$(nproc) && make install
    
# update OpenGV
cd /usr/local/src/opengv
git checkout master && git pull
cd build
cmake .. \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr/local \
    -DBUILD_TESTS=OFF \
    -DBUILD_PYTHON=ON \git checkout master && 
    -DBUILD_ARCH=native \
    && make -j$(nproc) && make installros/catkin/releases
    
# update OpenSfM
cd /usr/local/src/OpenSfM
git checkout master && git pull
pip install -r requirements.txt
rm -rf build cmake_build
python setup.py build

# update Catkin
cd /usr/local/src/catkin
git checkout master && git pull && git checkout 0.7.12
cd build
cmake .. \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr/local \
    -DCATKIN_ENABLE_TESTING=OFF \
    && make -j$(nproc) && make installgit checkout master && 
    
# update Hexer
cd /usr/local/src/hexer
git checkout master && git pull
cd build
cmake .. \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr/local \
    && make -j$(nproc) && make install
    
# update Ecto
cd /usr/local/src/ecto
git checkout master && git pull
cd build
cmake .. \
    -DCMAKE_INSTALL_PREFIX:PATH=/usr/local \
    -DBUILD_DOC=OFF \
    -DBUILD_SAMPLES=OFF \
    -DCATKIN_ENABLE_TESTING=OFF \
    && make -j$(nproc) && make install

# build LASzip outside SuperBuild cmake
#WORKDIR /usr/local/src
#RUN git clone https://github.com/LASzip/LASzip.git
#RUN cd ./LASzip && \
#    git checkout master
#RUN cd ./LASzip && mkdir build && cd build && \
#    cmake .. \
#    -DCMAKE_INSTALL_PREFIX=/usr/local \
#    && make -j$(nproc) && make install

# update PDAL
# https://www.pdal.io/development/compilation/unix.html
# master produces gdal errors as of may122018
cd /usr/local/src/PDAL && \
git checkout master && git pull 
#&& git checkout 1.7.2
# need to run cmake twice to get make target generated - see PDAL unix install docs
# cd build && cmake -G "Unix Makefiles" .. && cmake .. \
cd build && cmake -G "Unix Makefiles" .. && cmake .. \
    -DCMAKE_INSTALL_PREFIX=/usr/local \
    -DBUILD_PGPOINTCLOUD_TESTS=OFF \
    -DBUILD_PLUGIN_PCL=ON \
    -DBUILD_PLUGIN_PGPOINTCLOUD=ON \
    -DBUILD_PLUGIN_CPD=OFF \
    -DBUILD_PLUGIN_GREYHOUND=OFF \
    -DBUILD_PLUGIN_HEXBIN=ON \
    -DBUILD_PLUGIN_ICEBRIDGE=OFF \
    -DBUILD_PLUGIN_MRSID=OFF \
    -DBUILD_PLUGIN_NITF=OFF \
    -DBUILD_PLUGIN_OCI=OFF \
    -DBUILD_PLUGIN_SQLITE=OFF \
    -DBUILD_PLUGIN_RIVLIB=OFF \
    -DBUILD_PLUGIN_PYTHON=OFF \
    -DENABLE_CTEST=OFF \
    -DWITH_LAZPERF=OFF \
    -DWITH_LASZIP=OFF \
    -DWITH_TESTS=OFF && \
    make -j$(nproc) && make install
    
# update Lidar2dem
# http://applied-geosolutions.github.io/lidar2dems/doc/installation.html
# need to update setup.py --> gdal==2.1.2 or just plain gdal
cd /usr/local/src/lidar2dems
git checkout master && git pull
./setup.py install

