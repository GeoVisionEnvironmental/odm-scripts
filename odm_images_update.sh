#!/bin/bash -xv

# access & update docker source-build container
# check if running, else start-up from image of latest
# docker --rm --name odm-source-builds-amd64 geovisionenvironmental/odm-source-builds-amd64

docker exec -it odm-source-builds-amd64-atom /usr/local/src/scripts/odm_source-builds_update.sh

docker commit odm-source-builds-amd64-atom geovisionenvironmental/odm-source-builds-amd64-atom:latest


# docker build --tag geovisionenvironmental/odm-amd64-dev-multi-atom:latest -f dockerfile.odm.multi-stage .
